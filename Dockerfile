FROM python:3.7
RUN apt-get update && apt-get install libopus0 -y
COPY . /app
WORKDIR /app
RUN chmod +x ./install.sh
RUN ./install.sh
CMD python3 -m csbot