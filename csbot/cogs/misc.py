import discord
from discord.ext import commands
import requests
import json
import datetime as dt
from csbot import logger_setup
import asyncio
from git import Repo

class NoBookException(Exception):
    pass

class Misc(commands.Cog):
    def __init__(self, bot: discord.ext.commands.Bot):
        self.bot = bot
        self.logger = logger_setup(self.__class__.__name__)
        self.packturl = "https://www.packtpub.com/packt/offers/free-learning"

    def __str__(self):
        return "Miscellaneous"

    @commands.command()
    @commands.has_any_role("Instructors", "Admin")
    async def clear(self, ctx, count):
        """Clear a chat channel of N lines"""
        with ctx.typing():
            count = int(count)
            await ctx.message.channel.purge(limit=count)
            self.logger.debug(f"Cleared {count} messages from channel {ctx.message.channel} in server {ctx.message.guild}")
            outmsg = await ctx.send(f"✅Cleared {count} messages")
        await asyncio.sleep(5)
        await outmsg.delete()

    @commands.command()
    async def git(self, ctx):
        """Show the url for the github repository"""
        r = Repo('.')
        url = list(r.remotes.origin.urls)[0]
        await ctx.send(f"View my git repo at {url}")
        self.logger.info(f"Sent git info to channel #{ctx.message.channel.name} in {ctx.message.server.name}")

    @commands.command()
    async def packt(self, ctx):
        """Show the free book of the day from Packtpub publishing"""
        try:
            with ctx.typing():
                utc_today = dt.datetime.utcnow().replace(hour=0, minute=0, second=0, microsecond=0)
                self.logger.debug("getting packt info")
                response = requests.get("https://services.packtpub.com/free-learning-v1/offers", params={
                    'dateFrom': utc_today.isoformat(),
                    'dateTo': (utc_today + dt.timedelta(days=1)).isoformat()
                })
                free_learning_offer = response.json()['data'][0]
                product_id = free_learning_offer['productId']
                product_summary = json.loads(requests.get(f"https://static.packt-cdn.com/products/{product_id}/summary").content)
                self.logger.debug(f"got product summary:{product_summary}")
                self.logger.debug(f"got free learning offers: {free_learning_offer}")
                embedout = discord.Embed()
                embedout.title = product_summary['title']
                embedout.type = "rich"
                expiresAt = dt.datetime.strptime(free_learning_offer['expiresAt'], "%Y-%m-%dT%H:%M:%S.%fZ")
                timeleft = expiresAt - dt.datetime.utcnow()
                embedout.url = self.packturl
                imgurl = product_summary['coverImage']
                embedout.set_image(url=imgurl)
                embedout.set_footer(text=f"Time left: {str(timeleft)} (expires at {expiresAt} UTC)")
                embedout.description = product_summary['oneLiner']
                self.logger.debug(str(embedout.to_dict()))
                await ctx.send(embed=embedout)
                self.logger.info(f"Sent packt info to channel #{ctx.message.channel.name} in {ctx.message.guild.name}")
        except Exception as e:
            import traceback
            traceback.print_exc()
            self.logger.error(e)


def setup(bot: discord.ext.commands.Bot):
    bot.add_cog(Misc(bot))

