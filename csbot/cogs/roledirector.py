import discord
from discord.ext import commands
from csbot import logger_setup
from os import environ


class RoleDirector(commands.Cog):
    def __init__(self, bot: discord.ext.commands.Bot):
        self.bot = bot
        self.logger = logger_setup(self.__class__.__name__)
        self.student_role = None
        self.alumni_role = None
        self.admin_role = None

    @commands.command(name='graduate', pass_context=True)
    async def graduate(self, ctx: discord.ext.commands.Context):
        """Promote yourself from Student to Alumni"""
        if not ctx.guild:
            self.logger.warn("'\\graduate' attempted outside of guild channel")
            await ctx.send("I'm unable to determine what server you're from. Try running this command in your server's bot channel!")

        self.logger.info(f"{ctx.message.author.name} asked for graduation")
        self.student_role = discord.utils.get(ctx.guild.roles, name="Students")
        self.alumni_role = discord.utils.get(ctx.guild.roles, name="Alumni")
        self.admin_role = discord.utils.get(ctx.guild.roles, name="Admin")
        member: discord.Member = ctx.message.author

        try:
            if self.student_role in member.roles and self.alumni_role not in member.roles:
                await member.remove_roles(self.student_role)
                await member.add_roles(self.alumni_role)
                await member.send("You are now graduated!")
            else:
                await member.send("Whoops! Looks like you're not a student, otherwise i'd promote you to alumni.")
        except discord.Forbidden or discord.HTTPException as e:
            self.logger.error(f"Unable to modify roles: {e}")
            await member.send(f"Whoops! I had troubles modifying your roles, contact {(await self.bot.application_info()).owner.mention} for help.")

    @commands.command(name='namechange', pass_context=True)
    async def namechange(self, ctx):
        """Set your nickname and choose a role"""
        self.logger.info(f"{ctx.message.author.name} asked for namechange")
        self.student_role = discord.utils.get(ctx.guild.roles, name="Students")
        self.alumni_role = discord.utils.get(ctx.guild.roles, name="Alumni")
        self.admin_role = discord.utils.get(ctx.guild.roles, name="Admin")
        await self._namechange(ctx.message.author, ctx.message.guild)

    async def _namechange(self, member: discord.Member, server: discord.Guild):
        role_options = {
            '🇦': ("Student", self.student_role),
            '🇧': ("Alumni", self.alumni_role),
        }

        role_menu_format = """
What are you?
{}
        """
        name_menu_format = """
How would you like your name displayed?
{}
        """
        app_info = await self.bot.application_info()
        owner = app_info.owner
        owner_channel = owner.dm_channel or await owner.create_dm()

        init_msg: discord.Message = await member.send("What is your first name?")
        dm_channel: discord.DMChannel = init_msg.channel
        first_name_msg = await self.bot.wait_for('message', check=lambda
            m: m.channel == dm_channel and m.author == dm_channel.recipient)
        await member.send("What is your last name?")
        last_name_msg = await self.bot.wait_for('message', check=lambda
            m: m.channel == dm_channel and m.author == dm_channel.recipient)
        name_options = {
            '🇦': f"{first_name_msg.content} {last_name_msg.content[0]}",
            '🇧': f"{first_name_msg.content} {last_name_msg.content}"
        }
        try:
            if self.student_role not in member.roles and self.alumni_role not in member.roles:
                role_menu: discord.Message = await member.send(role_menu_format.format(
                    '\n'.join([f"{str(option)}: {str(role_options[option][0])}" for option in role_options.keys()])))

                for option in role_options.keys():
                    await role_menu.add_reaction(option)

                reaction, user = await self.bot.wait_for('reaction_add', check=lambda r, u: u == dm_channel.recipient)
                await member.add_roles(role_options[reaction.emoji][1])
        except discord.Forbidden as e:
            self.logger.error(f"Unable to modify roles: {e}")
            await member.send(f"Whoops! I was unable to modify your roles. Contact {owner.mention} for help.")

        try:
            name_menu = await member.send(name_menu_format.format(
                '\n'.join([f"{str(option)}: {str(name_options[option])}" for option in name_options.keys()])))
            for option in name_options.keys():
                await name_menu.add_reaction(option)
            reaction, user = await self.bot.wait_for('reaction_add', check=lambda r, u: u == dm_channel.recipient)
            await member.edit(reason="User requested namechange", nick=name_options[reaction.emoji])
        except discord.Forbidden as e:
            self.logger.error(f"Unable to modify nickname: {e}")
            await member.send(f"Whoops! I was unable to set your nickname due to a permissions problem. Contact {owner.mention} for help.")
        else:
            await member.send("Thank you! You should now be able to access all chat channels on the server.")
            default_channel: discord.TextChannel = None
            for channel in server.channels:
                if isinstance(channel, discord.TextChannel) and channel.name.lower() == "general":
                    default_channel = channel
                    break
            self.logger.info(f"{member.name} is now known as {member.nick}. Name change successful.")
            await default_channel.send(f"{member.name} is now known as {member.nick}. Welcome, {member.mention}!")

    async def on_member_join(self, member: discord.Member):
        """Says when a member joined."""
        if member.guild.id == environ.get('CSMS_DISCORD_SERVER_ID'):
            self.logger.info(f"{member.name} joined {member.guild.name}")
            await self._namechange(member, member.server)


def setup(bot):
    bot.add_cog(RoleDirector(bot))
