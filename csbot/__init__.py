from discord.ext import commands
import discord
import pkgutil
from pymongo import MongoClient
import pymongo.errors
import pymongo.uri_parser
import logging
import sys
import csbot.cogs
from git import Repo
from git import exc as gitexc
import traceback

## ENV VARS
CSBOT_MONGODB_URL = None
CSBOT_LOGLEVEL = None
CSBOT_DISCORD_API_TOKEN = None
## /ENV VARS

description = '''Chat Bot For The CMU CS Discord Server
There are a number of utility commands being showcased here.'''

# this specifies what extensions to load when the bot starts up (from this directory)
cogs_dir = "cogs"
DISABLED_COGS_LIST = []

bot = commands.Bot(command_prefix='\\', description=description)


def package_contents(package):
    prefix = package.__name__ + "."
    return set(modname for importer, modname, ispkg in pkgutil.iter_modules(package.__path__, prefix))


@bot.event
async def on_ready():
    logger = get_logger()
    logger.debug(f"active log handlers: {', '.join([handler.__class__.__name__ for handler in logger.handlers])}")
    app_info = await bot.application_info()
    owner: discord.User = app_info.owner
    logger.info(f"Logged in as {bot.user.name} with id: '{bot.user.id}'.")
    logger.info(f"Owner of {bot.user.name} is {owner.name} with id: '{owner.id}'.")
    owner_channel: discord.DMChannel = owner.dm_channel or await owner.create_dm()
    await owner_channel.send("Bot Ready!")
    try:
        r = Repo('.')
        await owner_channel.send(f"""
Git Info:
URL: {list(r.remotes.origin.urls)[0]}
__Commit info__ (sha:{r.head.commit.hexsha})
Author: {r.head.commit.author}
GPG Signed: {r.head.commit.gpgsig != None}
Commit Message: {r.head.commit.message}
""")
    except gitexc.InvalidGitRepositoryError as e:
        traceback.print_exc()
        logger.error("The current working directory is not a valid git repo, therefore cannot get detailed git info")
        await owner_channel.send("Unable to read bot's git info from current directory")
    except Exception as e:
        logger.error(traceback.format_exc())

    joinlink = f"https://discordapp.com/oauth2/authorize?client_id={app_info.id}&scope=bot&permissions=0"
    joinmsg = f"This bot is currently not joined to any servers. Join me to a server by following this link: {joinlink}"
    logger.debug(f"Invite bot to servers using this link: {joinlink}")
    if len(bot.guilds) < 1:
        logger.warning(joinmsg)


@bot.command(hidden=True)
async def load(extension_name: str):
    """Loads an extension."""
    logger = get_logger()
    try:
        bot.load_extension(extension_name)
        logger.info(f"Loaded cog: {extension_name}")
    except (AttributeError, ImportError) as e:
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return
    await bot.say("{} loaded.".format(extension_name))


@bot.command(hidden=True)
async def unload(extension_name: str):
    """Unloads an extension."""
    bot.unload_extension(extension_name)
    await bot.say("{} unloaded.".format(extension_name))


def logger_setup(cog_name="csbot", log_level=logging.INFO):
    logger = logging.getLogger(cog_name)
    loghandler = logging.StreamHandler(stream=sys.stdout)
    logfilehandler = logging.FileHandler(f"{cog_name}.log", mode="a", encoding="utf-8")
    formatter = logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s')
    loghandler.setFormatter(formatter)
    logfilehandler.setFormatter(formatter)
    if len(logger.handlers) < 1:
        logger.addHandler(loghandler)
        logger.addHandler(logfilehandler)
    loglevels = {
        "CRITICAL": logging.CRITICAL,
        "ERROR": logging.ERROR,
        "WARNING": logging.WARNING,
        "WARN": logging.WARN,
        "INFO": logging.INFO,
        "DEBUG": logging.DEBUG,
    }
    loglevel = loglevels.get(CSBOT_LOGLEVEL.upper(), log_level)
    logger.setLevel(loglevel)
    for handler in logger.handlers:
        if isinstance(handler, logging.FileHandler):
            logger.info(f"Logfile location: {handler.baseFilename}")
    return logger


def get_logger(cog_name="csbot"):
    return logging.getLogger(cog_name)


def get_dbclient():
    logger = get_logger()
    try:
        mongodb_url = CSBOT_MONGODB_URL
        dbclient = MongoClient(mongodb_url)
    except KeyError or TypeError or pymongo.errors.ConfigurationError as e:
        logger.error(f"Unable to setup database, missing or malformatted MongoDB URL")
        return None
    return dbclient


def run(token=""):
    logger = logger_setup()
    for package in package_contents(csbot.cogs):
        if str(package) not in DISABLED_COGS_LIST:
            try:
                bot.load_extension(package)
                logger.info(f"Loaded cog: {package}")
            except Exception as e:
                logger.error(f"Failed to load extension {package}.", e)
    try:
        token = CSBOT_DISCORD_API_TOKEN
    except KeyError as e:
        logger.error("API token required for discord api. Given token might have been incorrect.")
    bot.run(token)


@bot.event
async def on_command_error(ctx, error):
    logger = get_logger()
    logger.error(error)
    if isinstance(error, commands.CheckFailure):
        await ctx.send("You do not have permission to use this command.")
        logger.error(f"user {ctx.message.author} does not have permission")
        return
    return