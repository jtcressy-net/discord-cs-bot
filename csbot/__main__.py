import csbot
import os
import argparse


def main():
    parser = argparse.ArgumentParser(description=csbot.description)
    parser.add_argument("--token", help="Discord API token. Can also set CSBOT_DISCORD_API_TOKEN", default=os.environ.get("CSBOT_DISCORD_API_TOKEN", None))
    parser.add_argument("--mongodb-url", help="URI of MongoDB to use with bot. Use the format: 'mongodb://username:password@hostname:27017/database'. Can also set CSBOT_MONGODB_URL", default=os.environ.get("CSBOT_MONGODB_URL", None))
    parser.add_argument("--log-level", help="Log level of bot. One of CRITICAL, ERROR, WARNING, WARN, INFO, DEBUG. Defaults to INFO", default=os.environ.get("CSBOT_LOGLEVEL", "INFO"))
    args = parser.parse_args()
    csbot.CSBOT_MONGODB_URL = args.mongodb_url
    csbot.CSBOT_LOGLEVEL = args.log_level
    csbot.CSBOT_DISCORD_API_TOKEN = args.token
    print(csbot.CSBOT_LOGLEVEL, csbot.CSBOT_MONGODB_URL, csbot.CSBOT_DISCORD_API_TOKEN)
    print(args)
    csbot.run()


if __name__ == "__main__":
    main()
