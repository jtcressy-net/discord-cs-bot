from setuptools import setup


requirements = []
with open('requirements.txt') as f:
  requirements = f.read().splitlines()

setup(
    name="discord-cs-bot",
    version="0.1.0",
    packages=['csbot'],
    entrypoints={
        'console_scripts': [
            'csbot = csbot.__main__:main'
        ]
    },
    install_requires=requirements,
)
